const btn = document.querySelectorAll(".btn");
const display = document.getElementById("input");

var dValue = "0";
display.value = dValue;
var tempValue;
var lastValue;
var operator;
var operatorClicked = false;

btn.forEach((button) => {
  button.addEventListener("click", myfunction);

  function myfunction() {
    if (button.classList.contains("one")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 1;
        operatorClicked = false;
      } else {
        dValue = `${display.value}1`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("two")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 2;
        operatorClicked = false;
      } else {
        dValue = `${display.value}2`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("three")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 3;
        operatorClicked = false;
      } else {
        dValue = `${display.value}3`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("four")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 4;
        operatorClicked = false;
      } else {
        dValue = `${display.value}4`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("five")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 5;
        operatorClicked = false;
      } else {
        dValue = `${display.value}5`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("six")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 6;
        operatorClicked = false;
      } else {
        dValue = `${display.value}6`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("seven")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 7;
        operatorClicked = false;
      } else {
        dValue = `${display.value}7`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("eight")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 8;
        operatorClicked = false;
      } else {
        dValue = `${display.value}8`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("nine")) {
      if (display.value == "0" || operatorClicked == true) {
        display.value = 9;
        operatorClicked = false;
      } else {
        dValue = `${display.value}9`;
        display.value = dValue;
      }
    }
    if (button.classList.contains("zero")) {
      if (display.value == "0"|| operatorClicked == true) {
        display.value = 0;
        operatorClicked = false;
      } else {
        dValue = `${display.value}0`;
        display.value = dValue;
      }
    }

    if (button.classList.contains("dot")) {
      if (!display.value.includes(".")) {
        if (display.value == '0'|| operatorClicked == true) {
          let temp = Number("0.");
          display.value = "0.";
          operatorClicked = false;
        } else {
          dValue = `${display.value}.`;
          display.value = dValue;
        }
      }
    }

    if (button.classList.contains("clear")) {
      display.value = 0;
      operatorClicked = false;
    }
    

    if (button.classList.contains("plus")) {
      tempValue = display.value;
      operator = "plus";
      operatorClicked = true;
    }
    if (button.classList.contains("minus")) {
      tempValue = display.value;
      operator = "minus";
      operatorClicked = true;

    }
    if (button.classList.contains("divide")) {
      tempValue = display.value;
      operator = "divide";
      operatorClicked = true;
    }
    if (button.classList.contains("multiple")) {
      tempValue = display.value;
      operator = "multiple";
      operatorClicked = true;
    }
    if (button.classList.contains("equal")) {
      operatorClicked = true;

      lastValue = display.value;

      // display.value = Number(minusValue) - Number(lastValue);

      if (operator == "plus") {
        display.value = Number(tempValue) + Number(lastValue);
      }
      if (operator == "minus") {
        display.value = Number(tempValue) - Number(lastValue);
      }
      if (operator == "divide") {
        display.value = Number(tempValue) / Number(lastValue);
      }
      if (operator == "multiple") {
        display.value = Number(tempValue) * Number(lastValue);
      }
    }
  }
});
